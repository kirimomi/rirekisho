﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainSystem : MonoBehaviour
{

    public static AudioSource Audio;

    Page[] m_page;

    [SerializeField] Sprite[] m_sprite;

    [SerializeField] Image m_image;
    [SerializeField] Text m_title;
    [SerializeField] Text m_text;

    [SerializeField] Button m_buttonNext;
    [SerializeField] Button m_buttonPrev;
    [SerializeField] Button m_buttonYoutube;

    [SerializeField] AudioClip m_se;


    string m_url;

    static int m_pageNum = 0;

    struct Page
    {
        public Sprite Image;
        public string Title;
        public string Text;
        public string Url;
    }

    void Awake()
    {
#if UNITY_ANDROID
        //ステータスバーの色を変える
        ApplicationChrome.statusBarColor = 0xffff0000;
        //ナビゲーションバーの色を変える
        ApplicationChrome.navigationBarColor = 0x8800ff00;
        //ステータスバーを変更する
        ApplicationChrome.statusBarState = ApplicationChrome.States.TranslucentOverContent;
        //ナビゲーションバーを変更する
        ApplicationChrome.navigationBarState = ApplicationChrome.States.Visible;
        ApplicationChrome.navigationBarColor = 0xff000000;
#endif
        Time.timeScale = 1f;
        Application.targetFrameRate = 60;
    }

    void Start()
    {
        Audio = GetComponent<AudioSource>();
        InitData();
        ShowPage();
        StartCoroutine(iMain());
    }

    IEnumerator iMain()
    {
        while (true)
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                // アプリケーション終了
                Debug.Log("Esc Pressed");
                Application.Quit();
                yield break;
            }
            yield return null;
        }
    }



    void InitData()
    {
        m_page = new Page[12];

        m_page[0].Title = "松村 憲男";
        m_page[0].Text = "1976/4/14 富山県黒部市生まれ" + System.Environment.NewLine +
                         "富山県立富山中部高等学校 理数科 卒" + System.Environment.NewLine +
                         "東京大学 工学部 電気電子情報科 卒" + System.Environment.NewLine +
                         "有限会社アンブレラ" + System.Environment.NewLine +
                         "" + System.Environment.NewLine +
                         "趣味はゲームとランニング";
        m_page[0].Url = "https://www.youtube.com/channel/UCUF73VvwViY1C79KUcLkm_w/";
        m_page[0].Image = m_sprite[0];

        m_page[1].Title = "ピカチュウげんきでちゅう";
        m_page[1].Text = "・当時アルバイト" + System.Environment.NewLine +
                         "・ミニゲーム作成（シルエットクイズ）";
        m_page[1].Url = "https://www.youtube.com/results?search_query=%E3%83%94%E3%82%AB%E3%83%81%E3%83%A5%E3%82%A6%E3%81%92%E3%82%93%E3%81%8D%E3%81%A7%E3%81%A1%E3%82%85%E3%81%86";
        m_page[1].Image = m_sprite[1];

        m_page[2].Title = "ポケモンチャンネル" + System.Environment.NewLine +
                          "〜ピカチュウといっしょ〜";
        m_page[2].Text = "・プランニング全般" + System.Environment.NewLine +
                         "・プロトタイプ作成" + System.Environment.NewLine +
                         "・ゲーム内コンテンツ作成" + System.Environment.NewLine +
                         "・UI設計" + System.Environment.NewLine +
                         "・スクリプトで書けるものなんでも";
        m_page[2].Url = "https://www.youtube.com/results?search_query=%E3%83%9D%E3%82%B1%E3%83%A2%E3%83%B3%E3%83%81%E3%83%A3%E3%83%B3%E3%83%8D%E3%83%AB";
        m_page[2].Image = m_sprite[2];

        m_page[3].Title = "ニンテンドーDS" + System.Environment.NewLine +
                          "E3 テックデモ Pikahu";
        m_page[3].Text = "・プランニング全般" + System.Environment.NewLine +
                         "・ミニゲームチューニング";
        m_page[3].Url = "https://www.youtube.com/watch?v=lHGxSfxkY7I";
        m_page[3].Image = m_sprite[3];

        m_page[4].Title = "ポケモンダッシュ";
        m_page[4].Text = "・プランニング全般" + System.Environment.NewLine +
                         "・レベルデザイン" + System.Environment.NewLine +
                         "・チューニング";
        m_page[4].Url = "https://www.youtube.com/results?search_query=%E3%83%9D%E3%82%B1%E3%83%A2%E3%83%B3%E3%83%80%E3%83%83%E3%82%B7%E3%83%A5";
        m_page[4].Image = m_sprite[4];

        m_page[5].Title = "ポケモンPCマスター";
        m_page[5].Text = "・プランニング全般" + System.Environment.NewLine +
                         "・教材ステージ作成" + System.Environment.NewLine +
                         "・キャラ劇スクリプト";
        m_page[5].Url = "";
        m_page[5].Image = m_sprite[5];

        m_page[6].Title = "みんなのポケモン牧場";
        m_page[6].Text = "・プランニング全般" + System.Environment.NewLine +
                         "・プロトタイプ作成" + System.Environment.NewLine +
                         "・全体仕様作成" + System.Environment.NewLine +
                         "・UI設計";
        m_page[6].Url = "https://www.youtube.com/results?search_query=%E3%81%BF%E3%82%93%E3%81%AA%E3%81%AE%E3%83%9D%E3%82%B1%E3%83%A2%E3%83%B3%E7%89%A7%E5%A0%B4";
        m_page[6].Image = m_sprite[6];

        m_page[7].Title = "みんなのポケモン牧場" + System.Environment.NewLine +
                          "プラチナ対応版";
        m_page[7].Text = "・プランニング全般";
        m_page[7].Url = "https://www.youtube.com/results?search_query=%E3%81%BF%E3%82%93%E3%81%AA%E3%81%AE%E3%83%9D%E3%82%B1%E3%83%A2%E3%83%B3%E7%89%A7%E5%A0%B4";
        m_page[7].Image = m_sprite[7];

        m_page[8].Title = "乱戦！" + System.Environment.NewLine +
                          "ポケモンスクランブル";
        m_page[8].Text = "・ディレクション" + System.Environment.NewLine +
                         "・全体仕様作成" + System.Environment.NewLine +
                         "・UI設計" + System.Environment.NewLine +
                         "・プレイヤー挙動チューニング" + System.Environment.NewLine +
                         "・わざチューニング" + System.Environment.NewLine +
                         "・テキスト全般" + System.Environment.NewLine +
                         "・スクリプトで書けることなんでも";
        m_page[8].Url = "https://www.youtube.com/results?search_query=%E4%B9%B1%E6%88%A6%EF%BC%81+%E3%83%9D%E3%82%B1%E3%83%A2%E3%83%B3%E3%82%B9%E3%82%AF%E3%83%A9%E3%83%B3%E3%83%96%E3%83%AB";
        m_page[8].Image = m_sprite[8];

        m_page[9].Title = "スーパー" + System.Environment.NewLine +
                          "ポケモンスクランブル";
        m_page[9].Text = "・ディレクション" + System.Environment.NewLine +
                         "・全体仕様作成" + System.Environment.NewLine +
                         "・UI設計" + System.Environment.NewLine +
                         "・プレイヤー挙動チューニング" + System.Environment.NewLine +
                         "・わざチューニング" + System.Environment.NewLine +
                         "・イベントシーケンスの実装・フラグ管理" + System.Environment.NewLine +
                         "・幕間劇調整" + System.Environment.NewLine +
                         "・スクリプトで書けることなんでも";
        m_page[9].Url = "https://www.youtube.com/results?search_query=%E3%82%B9%E3%83%BC%E3%83%91%E3%83%BC%E3%83%9D%E3%82%B1%E3%83%A2%E3%83%B3%E3%82%B9%E3%82%AF%E3%83%A9%E3%83%B3%E3%83%96%E3%83%AB";
        m_page[9].Image = m_sprite[9];

        m_page[10].Title = "ポケモンスクランブル U";
        m_page[10].Text = "・ディレクション" + System.Environment.NewLine +
                          "・全体仕様作成" + System.Environment.NewLine +
                          "・UI設計" + System.Environment.NewLine +
                          "・プレイヤー挙動チューニング" + System.Environment.NewLine +
                          "・わざチューニング" + System.Environment.NewLine +
                          "・ステージ作成（一部）" + System.Environment.NewLine +
                          "・ステージ仕上げ" + System.Environment.NewLine +
                          "・全体を通してのバランス調整" + System.Environment.NewLine +
                          "・スクリプトで書けることなんでも";
        m_page[10].Url = "https://www.youtube.com/results?search_query=%E3%83%9D%E3%82%B1%E3%83%A2%E3%83%B3%E3%82%B9%E3%82%AF%E3%83%A9%E3%83%B3%E3%83%96%E3%83%AB+U";
        m_page[10].Image = m_sprite[10];

        m_page[11].Title = "みんなの" + System.Environment.NewLine +
                           "ポケモンスクランブル";
        m_page[11].Text = "・ディレクション" + System.Environment.NewLine +
                          "・全体仕様作成" + System.Environment.NewLine +
                          "・プレイヤー挙動チューニング" + System.Environment.NewLine +
                          "・わざチューニング" + System.Environment.NewLine +
                          "・スクリプトで書けることなんでも";
        m_page[11].Url = "https://www.youtube.com/results?search_query=%E3%81%BF%E3%82%93%E3%81%AA%E3%81%AE%E3%83%9D%E3%82%B1%E3%83%A2%E3%83%B3%E3%82%B9%E3%82%AF%E3%83%A9%E3%83%B3%E3%83%96%E3%83%AB";
        m_page[11].Image = m_sprite[11];

    }

    void ShowPage()
    {
        m_image.sprite = m_page[m_pageNum].Image;
        m_title.text = m_page[m_pageNum].Title;
        m_text.text = m_page[m_pageNum].Text;
        m_url = m_page[m_pageNum].Url;

        m_buttonNext.interactable = (m_pageNum < m_page.Length - 1);
        m_buttonPrev.interactable = (0 < m_pageNum);
        m_buttonYoutube.interactable = (m_url != "");

    }

    public void OnButtonNext()
    {
        m_pageNum = Mathf.Min(m_page.Length - 1, m_pageNum + 1);
        ShowPage();
        Audio.PlayOneShot(m_se);
    }

    public void OnButtonPrev()
    {
        m_pageNum = Mathf.Max(0, m_pageNum - 1);
        ShowPage();
        Audio.PlayOneShot(m_se);
    }

    public void OnButtonYoutube()
    {
        if (m_url != "")
        {
            Application.OpenURL(m_url);
        }
        Audio.PlayOneShot(m_se);
    }
    //n秒待つ
    public IEnumerator iWaitSecond(float sec)
    {
        float count = 0;
        while (count < sec)
        {
            yield return null;
            count += Time.deltaTime;

        }
    }

    void OnApplicationPause(bool status)
    {
        if (status)
        {
            Debug.Log("pause!");
        }
        else
        {
            Debug.Log("resume!");
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

}
