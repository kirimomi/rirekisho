using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class BuildCompressedPlayer
{

    [MenuItem("Build/Android")]
    static void BuildAndriod()
    {
        BuildPipeline.BuildPlayer(
            EditorBuildSettings.scenes,
            "export.apk",
            BuildTarget.Android,
            BuildOptions.CompressWithLz4);
    }


    [MenuItem("Build/iOS")]
    static void BuildiOS()
    {
        BuildPipeline.BuildPlayer(
            EditorBuildSettings.scenes,
            "Builds/iOS",
            BuildTarget.iOS,
            BuildOptions.CompressWithLz4 |
            BuildOptions.AutoRunPlayer |
            BuildOptions.AcceptExternalModificationsToPlayer
            );
    }

}
