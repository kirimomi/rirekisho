﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effect : MonoBehaviour
{

    Animation m_anim;
    void Awake()
    {
        m_anim = GetComponent<Animation>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!m_anim.isPlaying)
        {
            GameObject.Destroy(this.gameObject);

        }
    }
}
